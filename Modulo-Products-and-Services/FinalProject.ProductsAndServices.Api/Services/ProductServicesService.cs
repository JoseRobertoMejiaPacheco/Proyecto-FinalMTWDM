

using FinalProject.ProductsAndServices.Api.Repositories.Interface;
using FinalProject.ProductsAndServices.Api.Services.Interface;
using FinalProject.ProductsAndServices.Entities;

namespace FinalProject.ProductsAndServices.Api.Services;

public class ProductServicesService : IProductServicesService
{
    private readonly IProductsAndServicesRepository _productsOrServicesRepository;

    public ProductServicesService(IProductsAndServicesRepository productOrServiceRepository)
    {
        _productsOrServicesRepository = productOrServiceRepository;
    }

    public async Task<bool> DeleteAsync(int id)
    {
        return await _productsOrServicesRepository.DeleteAsync(id);
    }

    public async Task<List<Products_Services>> GetAllAsync()
    {
        return await _productsOrServicesRepository.GetAllAsync();
    }

    public async Task<Products_Services> SaveAsync(Products_Services productServices)
    {
        return await _productsOrServicesRepository.SaveAsync(productServices);
    }

    public async Task<List<Products_Services>> SearchAsync(string idOrDescription)
    {
        return await _productsOrServicesRepository.SearchAsync(idOrDescription);
    }

    public async Task<Products_Services> UpdateAsync(Products_Services productServices)
    {
        return await _productsOrServicesRepository.UpdateAsync(productServices);
    }
    
}
