using FinalProject.ProductsAndServices.Entities;


namespace FinalProject.ProductsAndServices.Api.Services.Interface;

public interface IProductServicesService
{
    Task<Products_Services> SaveAsync(Products_Services productServices);
    Task<List<Products_Services>> SearchAsync(string idOrDescription);
    Task<List<Products_Services>> GetAllAsync();
    Task<Products_Services> UpdateAsync(Products_Services productServices);
    Task<bool> DeleteAsync(int id);
    
}
