
using FinalProject.ProductsAndServices.Api.Responses;
using FinalProject.ProductsAndServices.Api.Services.Interface;
using FinalProject.ProductsAndServices.Entities;
using Microsoft.AspNetCore.Mvc;


namespace DeLaSalle.Invoices.Api.Controllers;


[ApiController]
public class ProductServicesController : ControllerBase
{

    private readonly IProductServicesService _productServicesService;


    public ProductServicesController(IProductServicesService productServicesService)
    {
        _productServicesService = productServicesService;
        
    }

    [HttpGet]
    [Route("api/[controller]")]
    public async Task<ActionResult<Response<IEnumerable<Products_Services>>>> GetAll()
    {
        var response = new Response<IEnumerable<Products_Services>>();
        var productsOrServices = await  _productServicesService.GetAllAsync();
        response.Data = productsOrServices;
        return Ok(response);

    }


    [HttpGet]
    [Route("api/[controller]/{idOrDescription}")]
    public async Task<ActionResult<Response<IEnumerable<Products_Services>>>> Search(string idOrDescription)
    {
        var response = new Response<List<Products_Services>>();
        var productOrService = await  _productServicesService.SearchAsync(idOrDescription);

        if(productOrService == null)
        {
            response.Message = "Product or Service not found";
            response.Errors.Add("The productOrService ID provided was not found");

            return NotFound(response);
        }

        response.Data = productOrService;

        return Ok(response);

    }


    [HttpPost]
    [Route("api/[controller]")]
    public async Task<ActionResult<Response<Products_Services>>> Save([FromBody] Products_Services productOrService)
    {
        var response = new Response<Products_Services>();
        productOrService = await _productServicesService.SaveAsync(productOrService);
        response.Data = productOrService;
        return Created("api/[controller]/",response);
    }

    [HttpPut]
    [Route("api/[controller]")]
    public async Task<ActionResult<Response<Products_Services>>> Update([FromBody] Products_Services productOrService)
    {
        var response = new Response<Products_Services>();
        productOrService = await _productServicesService.UpdateAsync(productOrService);
        response.Data = productOrService;
        return Ok(response);
    }


    [HttpDelete]
    [Route("api/[controller]/{id}")]
    public async Task<ActionResult<Response<bool>>> Delete(int id)
    {
        var response = new Response<bool>();
        response.Data = await  _productServicesService.DeleteAsync(id);
        
        return Ok(response);

    }



}