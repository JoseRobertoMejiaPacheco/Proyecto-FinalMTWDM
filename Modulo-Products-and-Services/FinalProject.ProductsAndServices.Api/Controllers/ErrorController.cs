using FinalProject.ProductsAndServices.Entities;
using Microsoft.AspNetCore.Mvc;
using FinalProject.ProductsAndServices.Api.Services.Interface;

namespace FinalProject.ProductsAndServices.Controllers;


[ApiController]
public class ErrorController : ControllerBase
{

    [Route("/api/error")]
    public IActionResult HandleError() => Problem();

}