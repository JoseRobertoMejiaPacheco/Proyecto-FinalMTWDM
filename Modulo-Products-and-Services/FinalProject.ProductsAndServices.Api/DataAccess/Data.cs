using System.Data;
using FinalProject.ProductsAndServices.Api.DataAccess.Interfaces;
using MySqlConnector;


namespace FinalProject.ProductsAndServices.Api.DataAccess;


public class Data : IData
{
    /*
    
    Representa un conjunto de propiedades de configuración de aplicación de clave/valor.
    Es decir representa cada uno de los objetos/nodos 
    localizados en el archivo DeLaSalle.Invoices.Api/appsettings.json
    */
    private readonly IConfiguration _configuration;
    //DatabaseConnection se encuentra en DeLaSalle.Invoices.Api/appsettings.json

    /*
    _connectionString representa el valor estático del nombre del nodo que
    contiene la cadena de conexion hacia la base de datos
    */
    private string _connectionString = "DatabaseConnection";

    /*
    MySqlConnection representa una conexión a una base de datos MySQL.
    */
    private MySqlConnection _conn;


    /*Inyeccion de Dependencia de IConfiguration*/
    public Data(IConfiguration configuration) 
    {
        _configuration = configuration;
    }


    /*
    Representa una conexión abierta a una fuente de datos y está implementada por datos .NET
    proveedores que acceden a bases de datos relacionales.
    */
    public IDbConnection DbConnection 
    {
        get
        {
            /*
            Si la conexión el nula va a retornar una objeto de Conexion hacia la base de datos
            */
            if(_conn == null)
            {
                _conn = new MySqlConnection(_configuration.GetConnectionString(_connectionString));
            }

            return _conn;
        }

    }
}