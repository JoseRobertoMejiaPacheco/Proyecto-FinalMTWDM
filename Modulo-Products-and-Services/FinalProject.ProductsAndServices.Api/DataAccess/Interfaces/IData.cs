using System.Data;


namespace FinalProject.ProductsAndServices.Api.DataAccess.Interfaces;


public interface IData
{
    /*
    Establece una sesión con una fuente de datos. 
    Permite abrir y cerrar conexiones, así como comenzar transacciones 
    (que se finalizan con los métodos Commit y Rollback de IDbTransaction. 
    Las clases SqlDbConnection y OleDbConnection implementan el interfaz de IDbConnection.
    */
    IDbConnection DbConnection{get;}
}