using FinalProject.ProductsAndServices.Api.DapperExtensions;
using FinalProject.ProductsAndServices.Api.DataAccess;
using FinalProject.ProductsAndServices.Api.DataAccess.Interfaces;
using FinalProject.ProductsAndServices.Api.MiddlewareExtension;
using FinalProject.ProductsAndServices.Api.Repositories.Interface;
using FinalProject.ProductsAndServices.Api.Repository;
using FinalProject.ProductsAndServices.Api.Services;
using FinalProject.ProductsAndServices.Api.Services.Interface;
using Microsoft.AspNetCore.Hosting;

var builder = WebApplication.CreateBuilder(args);

// Configuracion de Cors Permisiva
var corsPolicy = "corsPolicy";

builder.Services.AddCors(options => {
    options.AddPolicy(corsPolicy, configurePolicy => {
        configurePolicy.AllowAnyMethod().AllowAnyHeader().WithOrigins("*");
    });
});

// Add services to the container.
DapperExtensions.ConfigureSqlMapperExtensions("FinalProject.ProductsAndServices.Entities.");

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


builder.Services.AddSingleton<IConfiguration>(builder.Configuration);

builder.Services.AddScoped<IProductServicesService, ProductServicesService>();
builder.Services.AddScoped<IProductsAndServicesRepository, ProductsAndServicesRepository>();
builder.Services.AddScoped<IData, Data>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
    else // Production
    {
        app.UseExceptionHandler("/Error");
        // Remove to use HTTP only
        app.UseHsts(); // HTTPS Strict mode
    }
// app.UseHttpsRedirection();

//app.UseExceptionHandler("/api/error");

//Implementacion de un Extension Method para manejar las exepciones
app.ConfigureExceptionHandler();

app.UseCors(corsPolicy);

app.UseAuthorization();

app.MapControllers();

app.Run();



