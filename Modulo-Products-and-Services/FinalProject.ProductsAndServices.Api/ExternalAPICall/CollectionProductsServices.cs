﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace FinalProject.ProductsAndServices.Api.ExternalAPICall
{
    public class CollectionProductsServices
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }

        [BsonElement("idDb")]
        public string IdDb { get; set; }


        [BsonElement("description")]
        public string Description { get; set; }
    }
}
