﻿using FinalProject.ProductsAndServices.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;

namespace FinalProject.ProductsAndServices.Api.ExternalAPICall
{
    public class PostMongoAPI
    {
        public CollectionProductsServices PostItem(Products_Services products_Services)
        {
            string responseBody = "";
                //173658098949

                JObject rss =
       new JObject(
                   new JProperty("idDb", products_Services.Id),
                   new JProperty("description", products_Services.Description.ToString())
                   );
                var r = rss;
                Console.WriteLine(rss.ToString());
                var _url = $"http://54.151.10.35/productsandservices";
                var request = (HttpWebRequest)WebRequest.Create(_url);
                dynamic product = new JObject();
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Accept = "application/json";
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(rss);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                    using ( WebResponse response = request.GetResponse())
                    {
                        using (Stream strReader = response.GetResponseStream())
                        {
                            using (StreamReader objReader = new StreamReader(strReader))
                            {
                                responseBody = objReader.ReadToEnd().ToString();
                            }
                        }
                    }

            CollectionProductsServices employee = JsonConvert.DeserializeObject<CollectionProductsServices>(responseBody);



            return employee;
        }

    }
}
