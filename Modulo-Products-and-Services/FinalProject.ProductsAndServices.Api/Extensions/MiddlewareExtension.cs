

using System.Net;
using FinalProject.ProductsAndServices.Api.Responses;
using Microsoft.AspNetCore.Diagnostics;

namespace FinalProject.ProductsAndServices.Api.MiddlewareExtension;

public static class MiddlewareException
{
// IApplicationBuilder es la interfaz a la que se le va a agregar el extension method
// Define una clase que proporciona los mecanismos para 
// configurar la canalización de solicitudes de una aplicación.
    public static void ConfigureExceptionHandler(this IApplicationBuilder app)
    {

        app.UseExceptionHandler((appError) =>{

            appError.Run(async context =>{

                context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                context.Response.ContentType = "application/json";
                var contextFeature = context.Features.Get<IExceptionHandlerFeature>();

                if(contextFeature != null)
                {
                    var response = new Response<string>();   
                    response.Errors.Add(contextFeature.Error.Message);


                    await context.Response.WriteAsJsonAsync(response);
                }


            });
        });
    }
    
}