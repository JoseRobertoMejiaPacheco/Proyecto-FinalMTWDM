

namespace FinalProject.ProductsAndServices.Api.DapperExtensions;


public static class DapperExtensions
{

    public static void ConfigureSqlMapperExtensions(string nameSpace)
    {
        
        Dapper.Contrib.Extensions.SqlMapperExtensions.TableNameMapper = (entityName) => {

            var name = entityName.ToString();

            if(name.Contains(nameSpace))
            {
                name = name.Replace(nameSpace,string.Empty);
            }

            name = ToUpperFirstLetter(name).ToLower();

            return name;
        };
    }

    private static string ToUpperFirstLetter(string source)
    {
        if(string.IsNullOrEmpty(source))    
            return string.Empty;
        return char.ToUpper(source[0])+ source.Substring(1);
    }

}