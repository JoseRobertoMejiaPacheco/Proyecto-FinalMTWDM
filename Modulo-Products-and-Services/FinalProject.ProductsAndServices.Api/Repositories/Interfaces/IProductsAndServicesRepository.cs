

using FinalProject.ProductsAndServices.Entities;

namespace FinalProject.ProductsAndServices.Api.Repositories.Interface;

public interface IProductsAndServicesRepository
{
    Task<Products_Services> SaveAsync(Products_Services productOrService);
    Task<List<Products_Services>> SearchAsync(string idOrDescription );
    Task<List<Products_Services>> GetAllAsync();
    Task<Products_Services> UpdateAsync(Products_Services productOrService);
    Task<bool> DeleteAsync(int id);
}