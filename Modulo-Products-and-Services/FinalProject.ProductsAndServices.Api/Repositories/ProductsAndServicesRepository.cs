using FinalProject.ProductsAndServices.Api.DataAccess.Interfaces;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Dynamic;
using FinalProject.ProductsAndServices.Api.Repositories.Interface;
using FinalProject.ProductsAndServices.Entities;
using Confluent.Kafka;
using FinalProject.ProductsAndServices.Api.ExternalAPICall;

namespace FinalProject.ProductsAndServices.Api.Repository;

public class ProductsAndServicesRepository : IProductsAndServicesRepository
{

    private readonly IData _data;

    public ProductsAndServicesRepository(IData data)
    {
        _data = data;
    }

    public async Task<bool> DeleteAsync(int id)
    {
        return await _data.DbConnection.DeleteAsync(new Products_Services { Id = id });
    }

    public async Task<List<Products_Services>> GetAllAsync()
    {
        var sql = "SELECT * FROM products_and_services.products_services;";
                                 

        var productsOrServices = (await _data.DbConnection.QueryAsync<Products_Services>(sql)).ToList();

        return productsOrServices;

    }

    public async Task<Products_Services> SaveAsync(Products_Services productServices)
    {

        var sql = $"INSERT INTO `products_and_services`.`products_services` ( `description`) VALUES ('{productServices.Description}'); SET @id=last_insert_id(); select * from `products_and_services`.`products_services` where id =@id;";
      
        var productsOrServices = (await _data.DbConnection.QuerySingleAsync<Products_Services>(sql));
 
                var config = new ProducerConfig { BootstrapServers = "13.57.24.228:9093,13.57.24.228:9094,13.57.24.228:9095" };

                using (var producer = new ProducerBuilder<string, string>(config).Build())
                {
                    var key = productsOrServices.ComputedId.ToString() ;
                    string val = productsOrServices.Description.ToString();
                    var dr = await producer.ProduceAsync("productsandservices", new Message<string, string> { Key = key, Value = val });
                }
        PostMongoAPI postMongoAPI = new PostMongoAPI();
        postMongoAPI.PostItem(productsOrServices);


        return productsOrServices;

    }

    public async Task<List<Products_Services>> SearchAsync(string idOrDescription)
    {
        var sql = $"SELECT * FROM products_and_services.products_services WHERE  '{idOrDescription}' in (id) or description like '%{idOrDescription}%'";
      
        var productsOrServices = (await _data.DbConnection.QueryAsync<Products_Services>(sql)).ToList();

        return productsOrServices;
    }

    public async Task<Products_Services> UpdateAsync(Products_Services productServices)
    {
        await _data.DbConnection.UpdateAsync(productServices);

        return productServices;

    }
}