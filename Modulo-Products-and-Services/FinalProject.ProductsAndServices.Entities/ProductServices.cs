﻿using Dapper.Contrib.Extensions;

namespace FinalProject.ProductsAndServices.Entities;
public class Products_Services
{
    public int Id {get;set;}
    public string Description{get;set;}


    [Write(false)]
    public string ComputedId
    {        
        get
        {
            int decimalLength = Id.ToString("D").Length + 4;
            return Id.ToString("D" + decimalLength.ToString());
        }
    }
}
