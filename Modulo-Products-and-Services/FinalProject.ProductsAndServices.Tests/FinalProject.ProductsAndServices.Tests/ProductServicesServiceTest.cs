﻿using FinalProject.ProductsAndServices.Api.Repositories.Interface;
using FinalProject.ProductsAndServices.Api.Services;
using FinalProject.ProductsAndServices.Entities;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace FinalProject.ProductsAndServices.Tests
{
    public class ProductServicesServiceTest
    {
    [Fact]
    public async Task SaveAsync_WhenGetList_ReturnList()
    {
            // arrange 
        var expected = new Products_Services { Id = 2, Description = "Refresco Postobon 455 ml" };
        var mock = new Mock<IProductsAndServicesRepository>();
        mock.Setup(m => m.SaveAsync(new Products_Services { Id = 2, Description = "Refresco Postobon 455 ml" })).Returns(Task.FromResult(expected));
        var sut = new ProductServicesService(mock.Object);
        //act 
        var result = await sut.SaveAsync(expected);
        //assert 
        Assert.Equal(expected,result);

    }


        [Fact]
        public async Task SearchIdAsync_WhenGetList_ReturnList()
        {
            // arrange 
            var expected = new List<Products_Services>{
            new Products_Services{ Id = 1, Description = "Cerveza Colombiana 355 ml"}
        };
            var mock = new Mock<IProductsAndServicesRepository>();
            mock.Setup(m => m.SearchAsync("00000001")).Returns(Task.FromResult(expected));
            var sut = new ProductServicesService(mock.Object);
            //act 
            var result = await sut.SearchAsync("00000001");
            //assert 
            Assert.Equal(expected, result);

        }

        [Fact]
        public async Task SearchAsyncDescription_WhenGetList_ReturnList()
        {
            // arrange 
            var expected = new List<Products_Services>{
            new Products_Services{ Id = 1, Description = "Cerveza Colombiana 355 ml"},
            new Products_Services{ Id = 2, Description = "Cerveza Águila 355 ml"},
            new Products_Services{ Id = 3, Description = "Cerveza Club Colombia 355 ml"},
            new Products_Services{ Id = 4, Description = "Cerveza Corona 355 ml"}
        };
            var mock = new Mock<IProductsAndServicesRepository>();
            mock.Setup(m => m.SearchAsync("Cerv")).Returns(Task.FromResult(expected));
            var sut = new ProductServicesService(mock.Object);
            //act 
            var result = await sut.SearchAsync("Cerv");
            //assert 
            Assert.Equal(expected, result);

        }
        [Fact]
        public async Task GetAllAsync_WhenGetList_ReturnList()
        {
            // arrange 
            var expected = new List<Products_Services>{
            new Products_Services{ Id = 1, Description = "Cerveza Colombiana 355 ml"},
            new Products_Services { Id = 2, Description = "Refresco Postobon 455 ml"},
            new Products_Services { Id = 3, Description = "Oreo Sabaor Arequipe"},
            new Products_Services { Id = 4, Description = "Harina Para Arepas P.A.N"},
            new Products_Services { Id = 5, Description = "Aguardiente Antioqueño 750 ml"},
        };
            var mock = new Mock<IProductsAndServicesRepository>();
            mock.Setup(m => m.GetAllAsync()).Returns(Task.FromResult(expected));
            var sut = new ProductServicesService(mock.Object);
            //act 
            var result = await sut.GetAllAsync();
            //assert 
            Assert.Equal(expected, result);

        }
        [Fact]
        public async Task UpdateAsync_WhenGetList_ReturnList()
        {
            // arrange 
            var expected = new Products_Services { Id = 1, Description = "Cerveza Colombiana 355 ml" };
            var mock = new Mock<IProductsAndServicesRepository>();
            mock.Setup(m => m.UpdateAsync(new Products_Services { Id = 2, Description = "Refresco Postobon 455 ml" })).Returns(Task.FromResult(expected));
            var sut = new ProductServicesService(mock.Object);
            //act 
            var result = await sut.UpdateAsync(expected);
            //assert 
            Assert.Equal(expected, result);

        }

        [Fact]
        public async Task DeleteAsync_WhenGetList_ReturnList()
        {
            // arrange 
            var expected = true;
            var mock = new Mock<IProductsAndServicesRepository>();
            mock.Setup(m => m.DeleteAsync(1)).Returns(Task.FromResult(expected));
            var sut = new ProductServicesService(mock.Object);
            //act 
            var result = await sut.DeleteAsync(1);
            //assert 
            Assert.Equal(expected, result);

        }

    }
}
