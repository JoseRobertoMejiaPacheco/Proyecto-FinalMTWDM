using FinalProject.ProductsAndServices.Entities;
using Xunit;

namespace FinalProject.ProductsAndServices.Tests;

public class ProductServicesTest
{
    [Fact]
    public void Id_WhenSetValue_ReturnsSameValue()
    {
        //AAA Arranage Act Assert

        //Arranage Agregar Valores Esperados
        var expected = 64812;
        var sut = new Products_Services();
        //Act Verificacion de la Prueba
        sut.Id = expected;
        var result = sut.Id;
        //Assert Metodos de pruebas unitarias
        Assert.Equal(expected, result);
        //Si se pasa un valor a la propiedad se espera que el valor sea el mismo

    }

    [Fact]
    public void Description_WhenSetValue_ReturnsSameValue()
    {
        //AAA Arranage Act Assert

        //Arranage Agregar Valores Esperados
        var expected = "Cerveza Colombiana";
        var sut = new Products_Services();
        //Act Verificacion de la Prueba
        sut.Description = expected;
        var result = sut.Description;
        //Assert Metodos de pruebas unitarias
        Assert.Equal(expected, result);
        //Si se pasa un valor a la propiedad se espera que el valor sea el mismo

    }

    [Fact]
    public void ComputedId_WhenGetValue_ReturnsSameValue()
    {
        //AAA Arranage Act Assert

        //Arranage Agregar Valores Esperados
        var expected = "00001024";
        var sut = new Products_Services();
        //Act Verificacion de la Prueba
        sut.Id = 1024;
        var result = sut.ComputedId;
        //Assert Metodos de pruebas unitarias
        Assert.Equal(expected, result);
        //Si se pasa un valor a la propiedad se espera que el valor sea el mismo

    }
}