import {BrowserRouter,Route,Routes} from 'react-router-dom';

import Navigation from './components/navigation/Navigation';
import ProductAndServices from './containers/ProductAndServices';
import './navbar-top-fixed.css';


export default function App(){

    return (
        <BrowserRouter>

            <Navigation />
            <main className='container'>
                <Routes>

                    <Route path="/" element={<ProductAndServices/>}></Route>

                </Routes>
            </main>
        </BrowserRouter>
    
    );
}