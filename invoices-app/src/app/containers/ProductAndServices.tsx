import React, {useEffect} from 'react';
import {useStore} from '../store/store';
import {observer} from 'mobx-react-lite';
import ProductAndServicesDashboard from '../components/productsAndServices/ProductAndServicesDashboard';

const ProductAndServices= () =>{

    const {productAndServicesStore} = useStore();

    useEffect(()=>{
        productAndServicesStore.loadProdctsAndServices();
    },[productAndServicesStore])


    return(
        <React.Fragment>
            {productAndServicesStore.title}            
            <ProductAndServicesDashboard ></ProductAndServicesDashboard>

        </React.Fragment>
    )
}

export default observer(ProductAndServices);
