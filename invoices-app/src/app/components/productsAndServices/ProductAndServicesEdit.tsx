import React, { useState } from 'react';

import {useStore} from '../../store/store';
import {observer} from 'mobx-react-lite';
import IProductServices from '../../entities/productServices';


const ProductAndServicesEdit= () =>{

    const {productAndServicesStore} = useStore();
    
    const {selectedProductAndServicesStore,saveProductAndServicesEvent,cancelEditEvent} = productAndServicesStore;
    


    let defaultProductAndServices = {
        id : 0, 
        description : '',
        computedId : ''
        
    };

    let productAndServicesRef:IProductServices = (selectedProductAndServicesStore !=null)? selectedProductAndServicesStore: defaultProductAndServices;

    let label = productAndServicesRef.id === 0 ? 'New Product or Service': 'Edit ProdctsAndServices';

    const[productAndServices,setProductAndServices] = useState<IProductServices>(productAndServicesRef);

    const handleInputChange = (event:any) =>{
        const {name,value} = event.target;
        setProductAndServices({...productAndServices,[name]:value});
    }

    

    return(
        <React.Fragment>
            <h3 className="mt-3 mb-3">{label}</h3>
            <form className="row g-3 needs-validation" onSubmit={()=> saveProductAndServicesEvent(productAndServices)}>

  

                <div className="mb-3">
                    <label className="form-label">#</label>
                    <input onChange={handleInputChange} type="text" className="form-control" value={productAndServices.computedId} id="computedId" name="computedId" placeholder="XXXXXXXX" />
                </div>
                <div className="mb-3">
                    <label className="form-label">Description</label>
                    <input  onChange={handleInputChange} type="text" value={productAndServices.description} className="form-control" id="description" name="description" required />
                    

                </div>                
                <button type="submit"  className="btn btn-primary">Save</button>
                <button type="submit" onClick={ () => cancelEditEvent()} className="btn btn-dark"  >Cancel</button>
            </form>
        </React.Fragment>
    )
}

export default observer(ProductAndServicesEdit);
