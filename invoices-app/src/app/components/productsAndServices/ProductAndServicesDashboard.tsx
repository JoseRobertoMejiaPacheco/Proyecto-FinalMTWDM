
import React from 'react';
import {useStore} from '../../store/store';
import {observer} from 'mobx-react-lite';

import Loading from '../navigation/Loading';
import ErrorEmptyMessage from '../errors/ErrorEmptyMessage';
import ErrorApiMessage from '../errors/ErrorApiMessage';
import ProductAndServicesEdit from './ProductAndServicesEdit';
import ProductAndServicesDelete from './ProductAndServicesDelete';
import ProductAndServicesList from './ProductAndServicesList';

const ProductAndServicesDashboard= () =>{
    const {productAndServicesStore} = useStore();
    const {editProductAndServices,isApiError,isListLoaded,isDeletingProductAndServices,isLoading,isEmptyList} = productAndServicesStore;
    const {errorMessage} = productAndServicesStore;


    if(errorMessage.length){
        return <div>
                An error ocurred, please try again. {errorMessage}
               </div>;
    }

    return(
        <React.Fragment>
            {
                isLoading === true&&
                <Loading/>
            }
            {
                 editProductAndServices === false && isListLoaded === true && isDeletingProductAndServices===false && isLoading === false &&
                <ProductAndServicesList/>
            }
           {
               isEmptyList ===true&&
               <ErrorEmptyMessage/>
           }
            {
                editProductAndServices && isListLoaded === true && isDeletingProductAndServices===false&&
                <ProductAndServicesEdit />
            }
            {
                editProductAndServices === false && isListLoaded === true && isDeletingProductAndServices &&            
                 <ProductAndServicesDelete />
            }
            {
                isApiError === true &&
                <ErrorApiMessage />
            }
            
        </React.Fragment>
    )
}

export default observer(ProductAndServicesDashboard);
