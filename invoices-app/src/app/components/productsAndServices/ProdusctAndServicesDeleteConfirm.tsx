import React, { useState } from "react";

import { useStore } from "../../store/store";
import { observer } from "mobx-react-lite";
import IProductServices from "../../entities/productServices";

const ProdusctAndServicesDeleteConfirm = () => {
  const {productAndServicesStore} = useStore();
    
  const {selectedProductAndServicesStore,cancelDeleteEvent,deleteProductAndServicesEvent} = productAndServicesStore;


  let defaultProductAndServices = {
    id : 0, 
    description : '',
    computedId : ''
    
};

  let productAndServicesRef:IProductServices = (selectedProductAndServicesStore !=null)? selectedProductAndServicesStore: defaultProductAndServices;
  const[productAndServices] = useState<IProductServices>(productAndServicesRef);

  return (
    <React.Fragment>
      <div className="alert alert-warning" role="alert">
        Are you sure you want to delete this record for {productAndServices.description} with ID{" "}
        {productAndServices.id}?<hr></hr>
        <button
          type="button"
          onClick={() => deleteProductAndServicesEvent(productAndServices)}
          className="btn btn-warning"
        >
          Confirm
        </button>
        <button
          type="button"
          onClick={() => cancelDeleteEvent()}
          className="btn btn-dark m-2"
        >
          Cancel
        </button>
      </div>
    </React.Fragment>
  );
};

export default observer(ProdusctAndServicesDeleteConfirm);
