import React from "react";

import { useStore } from "../../store/store";
import { observer } from "mobx-react-lite";
import ProdusctAndServicesDeleteConfirm from "./ProdusctAndServicesDeleteConfirm";


const ProductAndServicesDelete = () => {
  const {productAndServicesStore} = useStore();
  const {isDeletingProductAndServices,isDeletingProductAndServiceSuccess} = productAndServicesStore;
  let label = 'Delete ProdctsAndServices...';


  return (
    <React.Fragment>
      <h3 className="mt-3 mb-3">{label}</h3>
      <form>
       
        {
            isDeletingProductAndServices === true &&isDeletingProductAndServiceSuccess === false&&
            <ProdusctAndServicesDeleteConfirm />
        }
      </form>
    </React.Fragment>
  );
};

export default observer(ProductAndServicesDelete);
