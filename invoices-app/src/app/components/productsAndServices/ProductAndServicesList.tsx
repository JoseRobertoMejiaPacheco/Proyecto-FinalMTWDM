
import React, { useState } from 'react';
import {useStore} from '../../store/store';
import {observer} from 'mobx-react-lite';
import IProductServices from '../../entities/productServices';
import IProductServicesFilter from '../../entities/http/productServicesFilter';



const ProductAndServicesList= () =>{

    const {productAndServicesStore} = useStore();
    const {productServices,loadProductsAndServicesByIdOrDescription,editProductAndServicesEvent,cancelDeleteProductsAndServicesEvent} = productAndServicesStore;
    const [filter,setFilters] = useState<IProductServicesFilter>({ idOrDescription: ''});
    const handleInputChanges = (event:any) =>{
        const {name,value} = event.target;
        console.log(event.target)
        setFilters({...filter,[name]:value})
        console.log(filter)
    }

    return(
        <React.Fragment>
        <h3 className="mt-3 mb-3">Product And Services</h3>

        <div className="row mb-3">
            <div className="input-group">
                <input name="idOrDescription" id="idOrDescription" type="text" 
                 onChange={handleInputChanges}
                className="form-control" placeholder="Id Or Description" aria-label="First name"/>

                <button className="btn btn-primary input-group-btn"  onClick = { () => loadProductsAndServicesByIdOrDescription(filter)}>
                    Search
                </button>

            </div>
           
        </div>
        <div className="row mb-3">
            <div className="col-md-12 text-right">
                <button onClick ={() => editProductAndServicesEvent(null)} type="button" className="btn btn-success">
                    New Product or Service
                </button>
            </div>
        </div>

        <table className="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Description</th>
            <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
        {
            productServices.map((productAndServices:IProductServices) =>(
                <tr key={productAndServices.id}>
                    <th scope="row">{productAndServices.computedId}</th>
                    <td>{productAndServices.description}</td>
                    <td>
                        
                        <button onClick={() => editProductAndServicesEvent(productAndServices)} type="button" className="btn btn-primary">
                            Edit
                        </button>
                    </td>
                    <td>                        
                        <button onClick={() => cancelDeleteProductsAndServicesEvent(productAndServices)} type="button" className="btn btn-danger">
                            Delete
                        </button>
                    </td>
                </tr>
            ))

        }
        
           
        </tbody>
        </table>
    </React.Fragment>
    )
}

export default observer(ProductAndServicesList);
