import React from "react";

import { useStore } from "../../store/store";
import { observer } from "mobx-react-lite";

const ErrorEmptyMessage = () => {
  const { productAndServicesStore } = useStore();
  const {cancelEditEvent } = productAndServicesStore;

  return (
    <React.Fragment>   
        <div className="alert alert-danger" role="alert">
        Not records found
          <hr></hr>
          <button
            type="button"
            onClick={() => cancelEditEvent()}
            className="btn btn-success m-2"
          >
            Try Again!!
          </button> 
        </div>    
 
    </React.Fragment>
  );
};

export default observer(ErrorEmptyMessage);