import React from "react";

import { useStore } from "../../store/store";
import { observer } from "mobx-react-lite";

const ErrorApiMessage = () => {
  const { productAndServicesStore } = useStore();
  const { apiError,cancelEditEvent} = productAndServicesStore;

  return (
    <React.Fragment>   
        <div className="alert alert-danger" role="alert">
        Error {apiError}
          <hr></hr>
          <button
            type="button"
            onClick={() => cancelEditEvent()}
            className="btn btn-success m-2"
          >
            Try Again!!
          </button> 
        </div>      
    </React.Fragment>
  );
};

export default observer(ErrorApiMessage);