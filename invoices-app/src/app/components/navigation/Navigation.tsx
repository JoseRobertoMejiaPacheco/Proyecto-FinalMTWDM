/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import {NavLink} from 'react-router-dom';


// Estructura HTML para realizar la parte gráfica de la navegación
const Navigation = () =>{

    return (
        <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <div className="container-fluid">
                <a className="navbar-brand" >Práctica Final</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarCollapse">
                <ul className="navbar-nav me-auto mb-2 mb-md-0">                
                    <li className="nav-item">
                        <NavLink className="nav-link active" to="/" >Product And Services</NavLink>
                    </li>
                    
                </ul>
                </div>
            </div>
        </nav>
    );

   
}

export default Navigation;
