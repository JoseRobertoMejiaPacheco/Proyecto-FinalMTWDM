import React, { useState } from "react";
import { observer } from "mobx-react-lite";
import { ClipLoader } from "react-spinners";

const Loading = () => {
  let [loading, setLoadingSpinner] = useState(true);
  let [color] = useState("#0000000");
  return (
    <React.Fragment>
    <div className="sweet-loading container d-flex align-items-center justify-content-center">
      <label onClick={() => setLoadingSpinner(true)} ></label>
      <ClipLoader color={color} loading={loading}  size={150} />
    </div>
    </React.Fragment>
  );
};

export default observer(Loading);