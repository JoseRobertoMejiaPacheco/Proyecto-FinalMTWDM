
import axios,{ AxiosResponse} from 'axios';

import IProductServices from '../entities/productServices';
import IProductServicesFilter from '../entities/http/productServicesFilter';

axios.defaults.baseURL = 'http://invoices.4gingenieria.com:4000/api/';


const responseBody = (response:AxiosResponse) => response.data;

const request = {
    get: (url:string, filters:{} = {}) => {
        const params = new URLSearchParams(filters);
        return axios.get(url,{ params }).then(responseBody)
    },
    post:(url:string,body:{}) => axios.post(url,body).then(responseBody),
    put:(url:string,body:{}) => axios.put(url,body).then(responseBody),
    delete:(url:string,id:number) => axios.delete(`${url}/${id}`).then(responseBody),
}

const ProdctsAndServices = {
    list: () => request.get('productservices'),
    search: (filters:IProductServicesFilter) => request.get(`productservices/${filters.idOrDescription}` ),
    create : (productServices:IProductServices) => request.post('productservices',productServices),
    update : (productServices:IProductServices) => request.put('productservices',productServices),
    delete: (productServices:IProductServices) => request.delete('productservices', productServices.id),
 }



const api = {
    axios,
    ProdctsAndServices
}

export default api;
