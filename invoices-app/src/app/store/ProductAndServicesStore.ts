import {makeAutoObservable} from 'mobx';


import api from '../api/api';

import IProductServicesFilter from '../entities/http/productServicesFilter';
import IProductServices from '../entities/productServices';


// Clase que contiene los métodos para comunicarse con la api
// ademas de tener los estados de ProdctsAndServices
export default class ProductAndServicesStore{

    title = '';
    editProductAndServices:Boolean = false;
    isListLoaded:Boolean = false;
    isEmptyList:Boolean = false;
    isLoading:Boolean = false;
    productServices: IProductServices[] = [];
    selectedProductAndServicesStore:IProductServices|null = null;
    errorMessage:String = '';
    apiError:String = '';
    isApiError:Boolean = false;

    isDeletingProductAndServiceSuccess:Boolean = false;
    isDeletingProductAndServiceError:Boolean = false
    isDeletingProductAndServices:Boolean = false



    constructor(){
        makeAutoObservable(this);
    }

    setTitle = () => {
        this.title = this.title + '!';
    }

    addError(error:String){
        this.errorMessage = error;
    }

    loadProdctsAndServices = async() =>{
        try{
            this.isApiError=false;
            this.isLoading = true;
            const responseProductAndService = await api.ProdctsAndServices.list();
            this.isListLoaded = true;
            this.isLoading = false;
            this.productServices = responseProductAndService.data;
            if (Object.keys(responseProductAndService.data).length === 0) {
                this.isEmptyList = true;                
              }
            else{
                this.isEmptyList = false;
            }
            
        }catch(error){
            this.isApiError=true;
            this.apiError=String(error);
            this.isLoading = false;       
            // this.addError(String(error));
        }
    }

    loadProductsAndServicesByIdOrDescription = async(filters:IProductServicesFilter) =>{
        try{
            this.isLoading = true;
            const responseProductAndService = await api.ProdctsAndServices.search(filters);
            this.isListLoaded = true;
            this.isLoading = false;
            this.productServices = responseProductAndService.data;    
            if (Object.keys(responseProductAndService.data).length === 0) {
                this.isEmptyList = true;
              }
            else{
                this.isEmptyList = false;
            }                    
        }catch(error){
            this.isApiError=true;
            this.apiError=String(error);
            this.addError(String(error));
        }
    }



    editProductAndServicesEvent = (productAndServices:IProductServices|null) =>{
        this.isEmptyList=false;
        this.editProductAndServices = true;
        this.selectedProductAndServicesStore = productAndServices;
    }

    cancelDeleteProductsAndServicesEvent = (productAndServices:IProductServices|null) =>{
        this.isDeletingProductAndServices = true;
        this.selectedProductAndServicesStore = productAndServices;
    }
    cancelEditEvent = () =>{
        this.editProductAndServices = false;
        this.selectedProductAndServicesStore = null;
        this.isEmptyList=false;
        this.loadProdctsAndServices()
    }
    cancelDeleteEvent = () =>{
        this.isDeletingProductAndServices = false;
        this.selectedProductAndServicesStore = null;
        this.isApiError=false;
    }

    saveProductAndServicesEvent = async(productAndServices:IProductServices) =>{
        try{
            console.log(productAndServices)
            if(productAndServices?.id === 0)
            {
                console.log(this.productServices)
                const response = await api.ProdctsAndServices.create(productAndServices);
                this.productServices.push(response.data);
                console.log(this.productServices)
            }
            else
            {
                
                await api.ProdctsAndServices.update(productAndServices);
                let index = this.productServices.findIndex( u=> u.id === productAndServices.id);
                this.productServices[index] = productAndServices;
                this.loadProdctsAndServices()
            }

            this.editProductAndServices = false;
            this.selectedProductAndServicesStore = null;

        }catch(error){
            this.isApiError=true;
            this.apiError=String(error);            
            this.addError(String(error));
        }
    }

    deleteProductAndServicesEvent = async(productAndServices:IProductServices) =>{
        try{
            console.log("ProdctsAndServices Obj Del")
            console.log(productAndServices)
            if(productAndServices?.id === 0)
            {
                const error="There is no productAndServices selected"
                this.addError(String(error));
            }
            else
            {
                await api.ProdctsAndServices.delete(productAndServices);
                // this.isDeletingProductAndServiceSuccess=true
                this.cancelDeleteEvent()
                this.loadProdctsAndServices()
                // let index = this.productServices.findIndex( u=> u.id === productAndServices.id);
                // this.productServices[index] = productAndServices;
            }

            this.editProductAndServices = false;
            this.selectedProductAndServicesStore = null;

        }catch(error){
            // this.addError(String(error));
            this.isApiError=true;
            this.apiError=String(error);
            this.isDeletingProductAndServiceError=true
        }
    }
    
}

