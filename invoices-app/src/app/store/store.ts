import {createContext,useContext} from 'react';
import ProductAndServicesStore from './ProductAndServicesStore';


interface IStore{
    productAndServicesStore: ProductAndServicesStore
}

export const store : IStore = {
    productAndServicesStore : new ProductAndServicesStore()
};

export const StoreContext = createContext(store);

export function useStore(){
    return useContext(StoreContext);
}
